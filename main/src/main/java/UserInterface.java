import kong.unirest.GenericType;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Scanner;

public class UserInterface {
	private static Logger logger = Logger.getLogger(UserInterface.class);
	private static ProjectManager manager = ProjectManager.getInstance();
	private static LocalizationManager localizationManager = LocalizationManager.getInstance();
	private Scanner scanner;
	private APIConnection API;


	public UserInterface() {
		scanner = new Scanner(System.in);
		localizationManager.setLanguage(Language.ENGLISH);
		logger.info(localizationManager.getValue("menu.main"));
		String input;
		API = APIConnection.getInstance();
		while ((input = scanner.nextLine()).matches("\\d") && !input.equals("exit")) {
			switch (input) {
				case "1":
					search(new GenericType<ArrayList<User>>() {
					}, new GenericType<User>() {
					}, manager.getValue("APIUsersPage"));
					break;
				case "2":
					search(new GenericType<ArrayList<Post>>() {
					}, new GenericType<Post>() {
					}, manager.getValue("APIPostsPage"));
				case "3":
					search(new GenericType<ArrayList<Photo>>() {
					}, new GenericType<Photo>() {
					}, manager.getValue("APIPhotosPage"));
				case "4":
					search(new GenericType<ArrayList<Comment>>() {
					}, new GenericType<Comment>() {
					}, manager.getValue("APICommentsPage"));
				case "5":
					search(new GenericType<ArrayList<Albums>>() {
					}, new GenericType<Albums>() {
					}, manager.getValue("APIAlbumsPage"));
				case "6":
					setLanguage();
				default:
			}
			logger.info(localizationManager.getValue("menu.main"));
		}
	}

	private <T> void search(GenericType<ArrayList<T>> arrayListGenericType, GenericType<T> genericType, String url) {
		logger.info(localizationManager.getValue("search.entities"));
		String input;
		while (((input = scanner.nextLine()).matches("\\d") || input.isEmpty()) && !input.equalsIgnoreCase("exit")) {
			logger.info(localizationManager.getValue("searching.process"));
			ArrayList<T> result = API.getEntities(arrayListGenericType, genericType, url, input);
			for (T x : result) {
				logger.info(x.toString());
			}
			logger.info(localizationManager.getValue("back.to.main.menu"));
		}
	}

	private void setLanguage() {
		String input;
		logger.info(localizationManager.getValue("menu.language"));
		while (!(input = scanner.nextLine()).equals("back")) {
			switch (input) {
				case "1":
					if (localizationManager.setLanguage(Language.ENGLISH))
						logger.info(localizationManager.getValue("language.set.success"));
					break;
				case "2":
					if (localizationManager.setLanguage(Language.RUSSIAN))
						logger.info(localizationManager.getValue("language.set.success"));
					break;
				default:
			}
			logger.info(localizationManager.getValue("menu.language"));
		}
	}
}
