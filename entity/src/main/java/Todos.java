public class Todos extends Entity {
	private int userId;
	private int id;
	private String title;
	private boolean completed;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int input) {
		this.userId = input;
	}

	public int getId() {
		return id;
	}

	public void setId(int input) {
		this.id = input;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String input) {
		this.title = input;
	}

	public boolean getCompleted() {
		return completed;
	}

	public void setCompleted(boolean input) {
		this.completed = input;
	}

	@Override
	public String toString() {
		return "Todos{" +
				"userId=" + userId +
				", id=" + id +
				", title='" + title + '\'' +
				", completed=" + completed +
				'}';
	}
}
