class Address {
	private String street;
	private String suite;
	private String city;
	private String zipcode;
	private Geo geo;

	public String getStreet() {
		return street;
	}

	public void setStreet(String input) {
		this.street = input;
	}

	public String getSuite() {
		return suite;
	}

	public void setSuite(String input) {
		this.suite = input;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String input) {
		this.city = input;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String input) {
		this.zipcode = input;
	}

	public Geo getGeo() {
		return geo;
	}

	public void setGeo(Geo input) {
		this.geo = input;
	}
}
