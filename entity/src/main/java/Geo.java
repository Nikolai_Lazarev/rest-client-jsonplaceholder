class Geo {
	private String lat;
	private String lng;

	public String getLat() {
		return lat;
	}

	public void setLat(String input) {
		this.lat = input;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String input) {
		this.lng = input;
	}
}
