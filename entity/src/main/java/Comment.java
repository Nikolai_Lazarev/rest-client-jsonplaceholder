public class Comment extends Entity {
	private int postId;
	private int id;
	private String name;
	private String email;
	private String body;

	public int getPostId() {
		return postId;
	}

	public void setPostId(int input) {
		this.postId = input;
	}

	public int getId() {
		return id;
	}

	public void setId(int input) {
		this.id = input;
	}

	public String getName() {
		return name;
	}

	public void setName(String input) {
		this.name = input;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String input) {
		this.email = input;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String input) {
		this.body = input;
	}

	@Override
	public String toString() {
		return "\n--------" + manager.getValue("entity.comment") + "--------" + "\n" +
				manager.getValue("entity.post.id") + ": " + postId + "\n" +
				manager.getValue("entity.id") + ": " + id + "\n" +
				manager.getValue("entity.name") + ": " + name + "\n" +
				manager.getValue("entity.email") + ": " + email + "\n" +
				body + "\n" +
				"------------------------";

	}
}
