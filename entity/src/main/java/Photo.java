public class Photo extends Entity {
	private int albumId;
	private int id;
	private String title;
	private String url;
	private String thumbnailUrl;

	public int getAlbumId() {
		return albumId;
	}

	public void setAlbumId(int input) {
		this.albumId = input;
	}

	public int getId() {
		return id;
	}

	public void setId(int input) {
		this.id = input;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String input) {
		this.title = input;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String input) {
		this.url = input;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String input) {
		this.thumbnailUrl = input;
	}

	@Override
	public String toString() {
		return "\n--------" + title + "--------" + "\n" +
				manager.getValue("entity.albums.id") + ": " + albumId + "\n" +
				manager.getValue("entity.id") + ": " + id + "\n" +
				"url" + ": " + url + "\n" +
				"------------------------";

	}
}
