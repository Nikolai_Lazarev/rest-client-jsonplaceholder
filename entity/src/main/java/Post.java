public class Post extends Entity {
	private int userId;
	private int id;
	private String title;
	private String body;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int input) {
		this.userId = input;
	}

	public int getId() {
		return id;
	}

	public void setId(int input) {
		this.id = input;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String input) {
		this.title = input;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String input) {
		this.body = input;
	}

	@Override
	public String toString() {
		return "\n--------"+title+"--------"+
				manager.getValue("entity.user.id")+ ": " + userId +"\n"+
				manager.getValue("entity.id")+ ": " + id +"\n"+
				body + "\n"+
				"------------------------";

	}
}
