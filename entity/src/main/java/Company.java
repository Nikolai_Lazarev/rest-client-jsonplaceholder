class Company {
		private String name;
		private String catchPhrase;
		private String bs;

		public String getName() {
			return name;
		}

		public void setName(String input) {
			this.name = input;
		}

		public String getCatchPhrase() {
			return catchPhrase;
		}

		public void setCatchPhrase(String input) {
			this.catchPhrase = input;
		}

		public String getBs() {
			return bs;
		}

		public void setBs(String input) {
			this.bs = input;
		}
	}
