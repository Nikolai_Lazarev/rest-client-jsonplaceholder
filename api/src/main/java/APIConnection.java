import kong.unirest.GenericType;
import kong.unirest.JacksonObjectMapper;
import kong.unirest.Unirest;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;

public class APIConnection {


	private static final ProjectManager manager = ProjectManager.getInstance();
	private static Logger logger = Logger.getLogger(APIConnection.class);
	private static APIConnection instance;

	public APIConnection() {
		Unirest.config().setObjectMapper(new JacksonObjectMapper());
	}

	public static APIConnection getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new APIConnection();
			return instance;
		}
	}

	public <T> ArrayList<T>  getEntities(GenericType<ArrayList<T>> arrayListGenericType, GenericType<T> genericType,  String url, String id) {
		ArrayList<T> result = new ArrayList<T>();
		if (id.isEmpty()) {
			result.addAll(Unirest.get(url).asObject(arrayListGenericType).getBody());
		} else {
			result.add(Unirest.get(url + id).asObject(genericType).getBody());
		}
		for (Iterator<T> iterator = result.iterator(); iterator.hasNext(); ) {
			logger.info(iterator.next());
		}
		return result;
	}
}
