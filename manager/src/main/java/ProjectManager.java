import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class ProjectManager {

	private static ProjectManager instance;
	private Properties properties;
	private static final String CONFIG_FILE = "config.properties";
	private static final Logger logger = Logger.getLogger(ProjectManager.class);

	private ProjectManager() {
		properties = new Properties();
		try {
			properties.load(getClass().getClassLoader().getResourceAsStream(CONFIG_FILE));
		} catch (IOException e) {
			logger.error(e + "config file is not find");
		}
	}

	public static ProjectManager getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new ProjectManager();
			return instance;
		}
	}

	public String getValue(String key) {
		return properties.getProperty(key);
	}
}
