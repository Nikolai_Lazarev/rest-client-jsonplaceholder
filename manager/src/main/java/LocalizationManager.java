import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class LocalizationManager {

	public static LocalizationManager instance;
	private Language language;
	private Properties properties;
	private static Logger logger = Logger.getLogger(LocalizationManager.class);

	private LocalizationManager(Language lang) {
		properties = new Properties();
		setLanguage(lang);
	}

	private LocalizationManager() {
		properties = new Properties();
		if (language != null) {
		} else {
			setLanguage(Language.ENGLISH);
		}
	}

	public boolean setLanguage(Language lang) {
		language = lang;
		boolean status = false;
		try {
			switch (language) {
				case ENGLISH:
					properties.load(getClass().getClassLoader().getResourceAsStream("English.properties"));
					status = true;
					break;
				case RUSSIAN:
					properties.load(getClass().getClassLoader().getResourceAsStream("Russian.properties"));
					status = true;
					break;
				default:
					language = Language.ENGLISH;
					properties.load(getClass().getClassLoader().getResourceAsStream("English.properties"));
					status = false;
					break;
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return status;
	}

	public static LocalizationManager getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new LocalizationManager();
		}
		return instance;
	}

	public Language getLanguage() {
		return language;
	}

	public String getValue(String key) {
		String result = properties.getProperty(key);
		try {
			result =  new String(result.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}
		return result;
	}
}
